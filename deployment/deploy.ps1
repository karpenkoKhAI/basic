Write-Host "Loading functions and inventory file" -ForegroundColor Yellow

#Load functions
Get-ChildItem "$PSScriptRoot\functions\*.ps1" | ForEach-Object { .$_ } | Out-Null

#Load inventory
. "$PSScriptRoot\inventory.ps1"

$ErrorActionPreference = "Stop"

try {

    #Create session
    $session = New-PSSession -SSHTransport -HostName $cmServer -UserName $cmUser -KeyFilePath $keyFile

    Write-Host "Pull repository`t[$repoUrl]" -ForegroundColor Cyan

    #Call function for download repository
    downloadRepository -repoUrl $repoUrl -repoBranch $repoBranch -repoMasterBranch $repoMasterBranch -destFolder $repoPath -session $session

    #Update application
    updateApplication -tfEnvPath $tfEnvPath -helmEnvPath $helmEnvPath -helmChartName $helmChartName -applicationName $applicationName -awsProfile $awsProfile -session $session

    if (Test-Path $keyFile) {
        Remove-Item $keyFile -Recurse -Force
    }

}
catch {
    Write-Error $_
}
finally {
    if (Test-Path $keyFile) {
        Remove-Item $keyFile -Recurse -Force
    }
    Remove-PSSession -Session $session
}

#Remove session
Remove-PSSession -Session $session
