#Define params for SSH session to CM instance
$cmServer = $env:CM_SERVER
$cmUser = 'ubuntu'
$keyFile = "$env:HOME/creds/$env:CI_PIPELINE_ID.pem"

#Define params for GitLab repo
$repoUrl = $env:TF_REPO_URL
$repoBranch = $env:TF_REPO_BRANCH
$repoMasterBranch = $env:TF_REPO_MASTER_BRANCH
$repoPath = "/home/$cmUser/$env:TF_REPO_PATH"

#Define vars for terraform
$tfEnvPath = "$repoPath/$env:TF_ENV_PATH"
$helmEnvPath = "$repoPath/$env:HELM_ENV_PATH"

#Application name
$applicationName = $env:APPLICATION_NAME
$helmChartName   = $env:HELM_CHART_NAME

#Define vars for AWS CLI commands
$awsProfile = $env:TF_AWS_PROFILE

if (-Not (Test-Path (Split-Path -Path $keyFile -Parent))) {
    New-Item -Path (Split-Path -Path $keyFile -Parent) -ItemType "directory" | Out-Null
}

if ("$env:CM_DEPLOY_SSH_KEY" -ne "") {
    "$env:CM_DEPLOY_SSH_KEY" | Out-File $keyFile
    chmod 700 $keyFile
}
