# Function name : updateApplication
# Description: 
#     This function should help to update k8s application.
# Input params:
#     tfEnvPath : specific path in project (terraform)
#     helmEnvPath : specific path in project (HELM)
#     helmChartName: specific HELM CHART name
#     applicationName: specific name of k8s application
#     awsProfile : specific AWS profile for this project
#     session : WinRm over SSH connect session

function updateApplication {
    param (
        [Parameter(Mandatory = $true)]
        [string] $tfEnvPath,
        [Parameter(Mandatory = $true)]
        [string] $helmEnvPath,
        [Parameter(Mandatory = $true)]
        [string] $helmChartName,
        [Parameter(Mandatory = $true)]
        [string] $applicationName,
        [Parameter(Mandatory = $true)]
        [string] $awsProfile,
        [Parameter(Mandatory = $true)]
        [System.Management.Automation.Runspaces.PSSession] $session

    )
    $block = {

        param($tfEnvPath,
            $helmEnvPath,
            $helmChartName,
            $applicationName,
            $awsProfile)

        Set-Location -Path "$tfEnvPath"

        tfenv install min-required #Install minimal required version
        tfenv use min-required #Use minimal required version

        terraform init
        if (!$?) { exit $LASTEXITCODE }

        Write-Host "Updating terraform configuration" -ForegroundColor Cyan

        terraform plan -out="tf_plan"
        if (!$?) { exit $LASTEXITCODE }

        terraform apply -input=false tf_plan
        if (!$?) { exit $LASTEXITCODE }

        Write-Host "Terraform configuration has been successfully updated" -ForegroundColor Cyan

        #Get ECS cluster ARN
        $k8sName = terraform output cluster_id

        #Get AWS region
        $k8sRegion = terraform output region

        #Update k8s config
        aws eks update-kubeconfig --name $k8sName --profile $awsProfile --region $k8sRegion
        if (!$?) { exit $LASTEXITCODE }

        Write-Host "Updating k8s Application [$applicationName]" -ForegroundColor Cyan

        #kubectl patch deployment "$applicationName-deployment" -p (-join("{\""spec\"":{\""template\"":{\""metadata\"":{\""annotations\"":{\""date\"":\""" , $(Get-Date -Format o).replace(':','-').replace('+','_') , "\""}}}}}"))
        helm dep update "$helmEnvPath/apps/"
        if (!$?) { exit $LASTEXITCODE }

        helm upgrade $helmChartName --set "$applicationName.enabled=true" "$helmEnvPath/apps"
        if (!$?) { exit $LASTEXITCODE }

        Write-Host "K8s application [$applicationName] has been updated" -ForegroundColor Cyan

    }

    Invoke-Command -Session $session -ScriptBlock $block -ArgumentList $tfEnvPath, $helmEnvPath, $helmChartName, $applicationName, $awsProfile
}
