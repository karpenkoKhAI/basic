
# Function name : downloadRepository
# Description: 
#     This function should help to download a project from SCM.
# Input params:
#     repoUrl : project URL
#     repoBranch: specific branch for project
#     destFolder : specific project path in local machine
#     session : WinRm over SSH connect session

#Load basic function
. "$PSScriptRoot\basic\invoke-git.ps1"

function downloadRepository {
    param (
        [Parameter(Mandatory = $true)]
        [string] $repoUrl,
        [Parameter(Mandatory = $true)]
        [string] $repoBranch,
        [Parameter(Mandatory = $true)]
        [string] $repoMasterBranch,
        [Parameter(Mandatory = $true)]
        [string] $destFolder,
        [Parameter(Mandatory = $true)]
        [System.Management.Automation.Runspaces.PSSession] $session
    )

    $invokeGitFunction = "function invokeGit {${function:invokeGit}}"

    $block = {

        param($destFolder,
            $repoUrl,
            $repoBranch,
            $repoMasterBranch,
            $invokeGitFunction)

        if (Test-Path $destFolder) {
            Remove-Item $destFolder -Recurse -Force
        }
        . ([ScriptBlock]::Create($invokeGitFunction))

        invokeGit -command "clone $repoUrl $destFolder"

        $is_branch_exist = $(invokeGit -command "rev-parse --verify --quiet origin/$repoBranch" -dirLocation $destFolder)
        if ($is_branch_exist) {
            invokeGit -command "checkout $repoBranch" -dirLocation $destFolder
        }
        else {
            invokeGit -command "checkout $repoMasterBranch" -dirLocation $destFolder
        }
    }

    Invoke-Command -Session $session -ScriptBlock $block -ArgumentList $destFolder, $repoUrl, $repoBranch, $repoMasterBranch, $invokeGitFunction
}
