
# Function name : invokeGit
# Description: 
#     This function should help to use git command because some git messages print in stderr stream.
# Input params:
#     command : project URL
#     dirLocation: specific project path in local machine

function invokeGit
{
    param(
        [Parameter(Mandatory = $true)]
        [string] $command,

        [string] $dirLocation
    )

    try {

        $exit = 0
        $path = [System.IO.Path]::GetTempFileName()
        
        if (($dirLocation -ne "") -and (Test-Path $dirLocation)) {

            Set-Location -Path "$dirLocation"
        }

        Invoke-Expression "git $command 2> $path"
        $exit = $LASTEXITCODE

        if ( $exit -gt 0 ) {
            Write-Error (Get-Content $path).ToString()
        }
        else {
            Get-Content $path | Select-Object -First 1
        }
    }
    catch {
        Write-Host "Error: $_`n$($_.ScriptStackTrace)"
    }
    finally {
        if ( Test-Path $path ) {
            Remove-Item $path
        }
    }
}
